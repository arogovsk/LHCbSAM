import logging
import urlparse

from ncgx.inventory import Checks, Hosts

HTTP_URIS = (
'https://eoslhcb.cern.ch:8443/eos/lhcb/test',
'https://f01-080-126-e.gridka.de:2880/pnfs/gridka.de/lhcb/test',
'https://ds-203-03-05.cr.cnaf.infn.it:8443/webdav/test',
'https://ds-203-03-07.cr.cnaf.infn.it:8443/webdav/test',
'https://f01-080-124-e.gridka.de:2880/pnfs/gridka.de/lhcb/test',
'https://ccdavlhcb.in2p3.fr:2880/lhcb/test',
'https://webdav-lhcbt1.pic.es:8446/lhcb/test',
'https://lcgcadm04.gridpp.rl.ac.uk/castor/test',
'https://se0003.m45.ihep.su:2880/lhcb/test',
'https://wasp1.grid.sara.nl:2882/pnfs/grid.sara.nl/data/lhcb/test',
'https://fly1.grid.sara.nl:2882/pnfs/grid.sara.nl/data/lhcb/test',
'https://se.cat.cbpf.br/dpm/cat.cbpf.br/home/lhcb/test',
'https://marsedpm.in2p3.fr/dpm/in2p3.fr/home/lhcb/test',
'https://storage01.lcg.cscs.ch:2880/pnfs/lcg.cscs.ch/lhcb/lhcb/test',
'https://grid05.lal.in2p3.fr/dpm/lal.in2p3.fr/home/lhcb/test',
'https://lpnse1.in2p3.fr/dpm/in2p3.fr/home/lhcb/test',
'https://bohr3226.tier2.hep.manchester.ac.uk/dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/test/',
'https://se.cis.gov.pl/dpm/cis.gov.pl/home/lhcb/test',
'https://tbit00.nipne.ro/dpm/nipne.ro/home/lhcb/test',
'https://heplnx237.pp.rl.ac.uk:2880/pnfs/pp.rl.ac.uk/data/lhcb/lhcb/test',
'https://gfe02.grid.hep.ph.ic.ac.uk:2880/pnfs/hep.ph.ic.ac.uk/data/lhcb/test',
#'https://t2dpm1-v6.physics.ox.ac.uk/dpm/physics.ox.ac.uk/home/lhcb/'
)

WEBDAV_METRICS = (
    'webdav.HTTP-TLS_CIPHERS-/lhcb/Role=production',
    'webdav.HTTP-DIR_HEAD-/lhcb/Role=production',
    'webdav.HTTP-DIR_GET-/lhcb/Role=production',
    'webdav.HTTP-FILE_PUT-/lhcb/Role=production',
    'webdav.HTTP-FILE_GET-/lhcb/Role=production',
    'webdav.HTTP-FILE_OPTIONS-/lhcb/Role=production',
    'webdav.HTTP-FILE_MOVE-/lhcb/Role=production',
    'webdav.HTTP-FILE_HEAD-/lhcb/Role=production',
    'webdav.HTTP-FILE_HEAD_ON_NON_EXISTENT-/lhcb/Role=production',
    'webdav.HTTP-FILE_PROPFIND-/lhcb/Role=production',
    'webdav.HTTP-FILE_DELETE-/lhcb/Role=production',
    'webdav.HTTP-FILE_DELETE_ON_NON_EXISTENT-/lhcb/Role=production'
)

log = logging.getLogger('ncgx')


def run():
    log.info("Processing webdav LHCb feed: %s" % len(HTTP_URIS))
    h = Hosts()
    c = Checks()
    for uri in HTTP_URIS:
        puri = urlparse.urlparse(uri)
        if puri.hostname:
            c.add('webdav.HTTP-All-/lhcb/Role=production', hosts=(puri.hostname,),
                  params={'args': {'--uri': uri}, '_unique_tag': 'HTTPS'})
            h.add(puri.hostname, tags=('HTTPS',))
            for metric in WEBDAV_METRICS:
                c.add(metric, hosts=(puri.hostname,), params={'_unique_tag': 'HTTPS'})
    h.serialize(fname='/etc/ncgx/conf.d/generated_hosts_webdav.cfg')
    c.serialize(fname='/etc/ncgx/conf.d/generated_webdav.cfg')

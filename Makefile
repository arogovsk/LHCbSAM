include VERSION

PKGNAME=nagios-plugins-wlcg-org.lhcb-$(VERSION)
TARFILE=$(PKGNAME).tgz

$(TARFILE):
	mkdir -p $(PKGNAME)
	cp -r usr $(PKGNAME)
	tar cvzf $(TARFILE) --owner=root --group=root $(PKGNAME)

clean:
	rm $(TARFILE)
	rm -fr $(PKGNAME)

rpm: nagios-plugins-wlcg-org.lhcb.spec $(TARFILE)
	rm -Rf RPMTMP
	mkdir -p RPMTMP/SOURCES RPMTMP/SPECS RPMTMP/BUILD \
            RPMTMP/SRPMS RPMTMP/RPMS/noarch RPMTMP/BUILDROOT
	cp -f $(TARFILE) RPMTMP/SOURCES
	export VERSION=$(VERSION) ; rpmbuild -ba    \
          --define "_topdir $(shell pwd)/RPMTMP"    \
          --buildroot $(shell pwd)/RPMTMP/BUILDROOT \
          nagios-plugins-wlcg-org.lhcb.spec
